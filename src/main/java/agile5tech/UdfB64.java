package agile5tech;

import io.confluent.ksql.function.udf.Udf;
import io.confluent.ksql.function.udf.UdfDescription;
import java.util.Base64;

@UdfDescription(name = "UdfB64", description = "encode / decode base64")
public class UdfB64 {

    @Udf(description = "decode base64 string to binary")
    public byte[] convert(String v1) {
	
	byte[] byteArray = Base64.getDecoder().decode(v1);
	return byteArray;
	
    }

    @Udf(description = "encode binary to base64 string")

    public String convert(byte[] v1) {

	String string = Base64.getEncoder().encodeToString(v1);
	return string;
    }
}
